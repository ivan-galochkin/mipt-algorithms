package algorithms;

public class Inversions {
    public static int num = 0;

    public Inversions() {
    }

    public static int[] merge(int[] a, int[] b) {
        int[] c = new int[a.length + b.length];
        int k = 0;
        int m = 0;
        int i = 0;

        while(k < a.length && m < b.length) {
            if (a[k] <= b[m]) {
                num += m;
                c[i] = a[k];
                ++k;
                ++i;
            } else {
                c[i] = b[m];
                ++m;
                ++i;
            }
        }

        while(k < a.length) {
            num += m;
            c[i] = a[k];
            ++k;
            ++i;
        }

        while(m < b.length) {
            c[i] = b[m];
            ++m;
            ++i;
        }

        return c;
    }

    public static int[] merge_sort(int[] c) {
        if (c.length <= 1) {
            return c;
        } else {
            int left_size = c.length / 2;
            int right_size = c.length - left_size;
            int[] left = new int[left_size];
            int[] right = new int[right_size];
            int k = 0;
            int m = 0;

            for(int i = 0; i < c.length; ++i) {
                if (i < left_size) {
                    left[k] = c[i];
                    ++k;
                } else {
                    right[m] = c[i];
                    ++m;
                }
            }

            left = merge_sort(left);
            right = merge_sort(right);
            return merge(left, right);
        }
    }
}
