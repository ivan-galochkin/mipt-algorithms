package algorithms;

public class Main {
    public static void main(String[] args) {
        int[] a = new int[]{4, 3, 5, 2, 1, 3, 2, 2};
        int[] res = SortingAlgorithms.quick_sort(a, 0, 7);

        for (int i = 0; i < a.length; ++i) {
            System.out.println(res[i]);
        }
    }
}
