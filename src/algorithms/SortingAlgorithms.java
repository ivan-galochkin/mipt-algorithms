package algorithms;

public class SortingAlgorithms {
    private static int[] arr;

    public SortingAlgorithms() {
    }

    private static int[] merge(int[] a, int[] b) {
        int[] c = new int[a.length + b.length];
        int k = 0;
        int m = 0;
        int i = 0;

        while (k < a.length && m < b.length) {
            if (a[k] <= b[m]) {
                c[i] = a[k];
                ++k;
                ++i;
            } else {
                c[i] = b[m];
                ++m;
                ++i;
            }
        }

        while (k < a.length) {
            c[i] = a[k];
            ++k;
            ++i;
        }

        while (m < b.length) {
            c[i] = b[m];
            ++m;
            ++i;
        }

        return c;
    }

    public static int[] merge_sort(int[] c) {
        if (c.length <= 1)
            return c;

        int left_size = c.length / 2;
        int right_size = c.length - left_size;
        int[] left = new int[left_size];
        int[] right = new int[right_size];
        int k = 0;
        int m = 0;

        for (int i = 0; i < c.length; ++i) {
            if (i < left_size) {
                left[k] = c[i];
                ++k;
            } else {
                right[m] = c[i];
                ++m;
            }
        }

        left = merge_sort(left);
        right = merge_sort(right);
        return merge(left, right);

    }

    private static void print(int[] a) {
        for (int i = 0; i < a.length; ++i) {
            System.out.print(a[i]);
            System.out.print(" ");
        }

        System.out.println();
    }

    private static int partition(int[] a, int l, int h) {
        int pivot = a[h];
        int i = l - 1;
        int j;
        for (j = l; j < h; ++j) {
            if (a[j] <= pivot) {
                ++i;
                int temp = a[i];
                a[i] = a[j];
                a[j] = temp;
            }
        }

        j = a[i + 1];
        a[i + 1] = a[h];
        a[h] = j;
        return i + 1;
    }

    public static int[] quick_sort(int[] a, int l, int h) {
        int[] stack = new int[h - l + 1];
        int top = -1;
        stack[++top] = l;
        stack[++top] = h;

        while (top >= 0) {
            h = stack[top--];
            l = stack[top--];
            int p = partition(a, l, h);
            if (p - 1 > l) {
                stack[++top] = l;
                stack[++top] = p - 1;
            }

            if (p + 1 < h) {
                stack[++top] = p + 1;
                stack[++top] = h;
            }
        }

        return a;
    }
}